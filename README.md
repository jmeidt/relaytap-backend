# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Jeff Meidt - TapQA


### Get relaytap-backend project ###
Download framework from bitbucket: 
* https://bitbucket.org/jmeidt/relaytap-backend/src/master/
### Initializing JMeter Tests ###

Drag your jmeter test into the `src/test/jmeter` folder.

use `mvn clean install` or `mvn clean verify` from the terminal to initiate jmeter tests.

Results appear in `target/jmeter/reports/`